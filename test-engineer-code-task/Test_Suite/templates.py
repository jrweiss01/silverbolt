MONITOR_FOLDER_DICT = {'file_path' : "", 'file_extension' : "", 'action' : "",  'result' : "PASS", \
                       'timestamp' : 0,'time_to_process' : 0, 'time_to_remove' : 0, 'Test_case_ID' : ""}

TEST_CASE_HEADER_DICT = {'Test_case_ID': "", 'test_case_name': ""}

TEST_CASE_RESULTS_DICT ={'Test_case_ID' : "", 'test_case_name' : "", 'testcase_status' : "",\
                    'CPU_usage' : "", 'mem_usage' : "", 'average_handling_time' : 0, 'median_handeling_time' : 0,\
                    'num_files_in_test' : 0, 'false_positive' : 0, 'positive_false' : 0,'errors_processing' : 0,
                    'errors_detecting' : 0, 'accuracy' : 0}

TEST_CASE_RESULTS_TUPLE = ['Test_case_ID', 'test_case_name', 'testcase_status', \
                     'CPU_usage', 'mem_usage', 'average_handling_time', 'median_handeling_time', \
                     'num_files_in_test','false_positive', 'positive_false', 'errors_processing', \
                      'errors_detecting', 'accuracy']

#id,path,status,processing time, removal time
FILE_RESULTS_TUPLE = ['Test_case_ID', 'file_path', 'result', 'time_to_process', 'time_to_remove']

DATE_FORMAT = "%a %d/%m/%Y %H:%M:%S"
