import sys
import time
import subprocess
import psutil
import copy
from templates import *
import threading
from Utils.files_and_directories_utils import *
from Utils.excel_utils import *

class AnalyzeTestResults:
    def __init__(self, path_to_aut):
        self.path_for_results = path_to_aut+"\\results"

    def median(self,lst):
        sortedLst = sorted(lst)
        lstLen = len(lst)
        index = (lstLen - 1) // 2

        if (lstLen % 2):
            return sortedLst[index]
        else:
            return (sortedLst[index] + sortedLst[index + 1]) / 2.0

    def analyze_after_file_creation(self, monitored_folder_actions, num_files_created):
        # print("analyzing test results")
        files_added = []
        files_found_clean = []
        files_found_dirty = []
        files_error = []
        files_changed = []
        files_removed = []
        processing_time_in_ms = []
        test_case_status = 'PASS'
        test_case_results = copy.deepcopy(TEST_CASE_RESULTS_DICT)
        file_creation_missed = 0


        for action in monitored_folder_actions:
            if action["action"] == "!ADDED!":
                files_added.append(action.copy())
            elif action["action"] == "!CLEAN!":
                files_found_clean.append(action.copy())
            elif action["action"] == "!DIRTY!":
                files_found_dirty.append(action.copy())
            elif action["action"] == "!CHANGED!":
                files_changed.append(action.copy())
            elif action["action"] == "!REMOVED!":
                files_removed.append(action.copy())
            elif action["action"] == "!ERROR!":
                files_error.append(action.copy())

        if len(files_added) != int(num_files_created):
            print("ERROR: not all files were created. Mising " + str(int(num_files_created) - len(files_added)) + " files")
            file_creation_missed = int(num_files_created) - len(files_added)
            test_case_status = 'FAILED'
        i = 0
        # This loop will go through all the files that were identified as added and collect data from the additional
        # Actions and analysis that is relevant to each file.
        for file_added in files_added:
            path, file_name = os.path.split(file_added["file_path"])
            file_name, file_extension = file_name.split(".")
            files_added[i]["file_extension"] = file_extension.strip("\n")
            found_in_clean_or_dirty = False
            was_file_removed = False
            # Go through the files marked with status clean and verify if they should be clean or if they should have
            # been recognized as dirt.  next calculate how much time it took to to evaluate if the file is clean or dirty.
            for file_marked_clean in files_found_clean:
                if file_marked_clean["file_path"] == file_added["file_path"]:
                    found_in_clean_or_dirty = True
                    if 'clean' in file_extension:
                        files_added[i]["result"] = 'PASS'
                    elif 'dirty' in file_extension:
                        test_case_status = 'FAILED'
                        files_added[i]["result"] = 'Positive False: dirty file identified as clean'
                        test_case_results["positive_false"] = int(test_case_results["positive_false"]) + 1
                    time_to_process = int(file_marked_clean["timestamp"]) - int(file_added["timestamp"])
                    files_added[i]["time_to_process"] = time_to_process
                    processing_time_in_ms.append(time_to_process)
                    break
            if found_in_clean_or_dirty == False:
                for file_marked_dirty in files_found_dirty:
                    if file_marked_dirty["file_path"] == file_added["file_path"]:
                        found_in_clean_or_dirty = True
                        if 'dirty' in file_extension:
                            files_added[i]["result"] = 'PASS'
                        elif 'clean' in file_extension:
                            files_added[i]["result"] = 'False Positive: clean file identified as dirty'
                            test_case_results["false_positive"] += 1
                            test_case_status = 'FAILED'
                        time_to_process = int(file_marked_dirty["timestamp"]) - int(file_added["timestamp"])
                        files_added[i]["time_to_process"] = time_to_process
                        processing_time_in_ms.append(time_to_process)
                        break
            for file_removed in files_removed:
                if file_removed["file_path"] == file_added["file_path"]:
                    was_file_removed = True
                    files_added[i]["time_to_remove"] = int(file_removed["timestamp"]) - int(file_added["timestamp"])
                    time_to_process = int(files_added[i]["time_to_remove"])
                    processing_time_in_ms.append(time_to_process)
                    break
            if was_file_removed == False and not 'Positive False' in files_added[i]["result"] and not 'clean' in file_extension:
                files_added[i]["result"] = 'Fail: Identified but not removed'
                test_case_status = 'FAILED'

            for error in files_error:
                if error["file_path"] == file_added["file_path"]:
                    if not files_added[i]["time_to_process"] > 0:
                        files_added[i]["time_to_process"] = int(error["timestamp"]) - int(file_added["timestamp"])
                    processing_time_in_ms.append(int(error["timestamp"]) - int(file_added["timestamp"]))

            i += 1
        j = 0
        for file_changed in files_changed:
            path, file_name = os.path.split(file_changed["file_path"])
            file_name, file_extension = file_name.split(".")
            files_changed[j]["file_extension"] = file_extension.strip("\n")
            found_in_removed = False
            was_file_removed = False
            dirty_files_chaged = 0
            for removed_file in files_removed:
                if file_changed["file_path"] == removed_file["file_path"]:
                    found_in_removed = True
                    if 'dirty' in file_extension:
                        dirty_files_chaged += 1
                        files_changed[j]["result"] = 'PASS'
                        time_to_process = int(removed_file["timestamp"]) - int(file_changed["timestamp"])
                        files_changed[j]["time_to_process"] = time_to_process
                        processing_time_in_ms.append(time_to_process)
                    else:
                        files_changed[j]["result"] = 'Fail: clean file removed'
                        test_case_status = 'FAILED'
                        time_to_process = int(removed_file["timestamp"]) - int(file_changed["timestamp"])
                        files_added[j]["time_to_process"] = time_to_process
                        processing_time_in_ms.append(time_to_process)
                    files_changed[j]["time_to_remove"] = time_to_process
            if found_in_removed == False:
                files_changed[j]["time_to_process"] = 0
                if 'dirty' in file_extension:
                    files_changed[j]["result"] = 'Fail: dirty file NOT removed'
                    dirty_files_chaged += 1
                    test_case_status = 'FAILED'
                else:
                    files_changed[j]["result"] = 'PASS'


            for error in files_error:
                if error["file_path"] == file_changed["file_path"]:
                    test_case_status = 'FAILED'
                    if not files_changed[j]["time_to_process"] > 0:
                        files_changed[j]["time_to_process"] = int(error["timestamp"]) - int(file_changed["timestamp"])
                    processing_time_in_ms.append(int(error["timestamp"]) - int(file_changed["timestamp"]))
            j += 1
        num_file_to_handle = 0
        if num_files_created > 0:
            num_file_to_handle = int(num_files_created)/2
        elif len(files_changed) > 0:
            num_file_to_handle = num_file_to_handle +  dirty_files_chaged
        else:
            test_case_results["accuracy"] = 'NA'
        test_case_results["accuracy"] = float(len(files_removed) / num_file_to_handle)
        test_case_results["testcase_status"] = test_case_status
        if len(processing_time_in_ms) > 0:
            test_case_results["average_handling_time"] = sum(processing_time_in_ms) / len(processing_time_in_ms)
            test_case_results["median_handeling_time"] = self.median(processing_time_in_ms)
        else:
            print("ERROR: processing time was not recorded")

        test_case_results["errors_processing"] = len(files_error)
        test_case_results["errors_detecting"] =  file_creation_missed
        # print("Finished Analyzing tet results")

        return test_case_results, files_added, files_changed


class ExecuteTestCases:
    def __init__(self,path_to_aut, current_os):
        self.path_to_aut = path_to_aut
        self.current_os = current_os
        self.test_results = []

    def generate_test_files(self, number_of_files, test_files_destination_path):
        #print("Generating new files")
        make_sure_folder_exists(test_files_destination_path)
        command = self.path_to_aut + ' -g ' + str(number_of_files) + ' -o ' + test_files_destination_path
        shell_output = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = shell_output.communicate()
        print("Output: " + stdout)
        if stderr != "":print(stderr)

    def edit_file_content(self,folder_path,num_files_to_edit):
        file_paths, file_names = FetchFiles(folder_path)
        if int(num_files_to_edit) > len(file_paths):
            num_files_to_edit = len(file_paths) - 1
        for i in range(0,int(num_files_to_edit)):
            if len(file_paths) > i:
                f = open(file_paths[i + 1], 'w')
                f.write("clean")
                f.close()

    def run(self,test_id, test_name, test_steps):
        os_monitor = MonitorOS(1, "Monitor OS Thread", self.current_os)
        analyze_results = AnalyzeTestResults(self.path_to_aut)
        test_steps_seprated = test_steps.split(",")
        num_files_to_generate = 0
        for test_step in test_steps_seprated:
            if 'generate files' in test_step.lower():
                step, num_files_to_generate, destination_path = test_step.split("::")
                self.generate_test_files(num_files_to_generate,destination_path)
            elif 'monitor folder' in test_step.lower():
                step,path_of_folder_to_watch = test_step.split("::")
                monitor_folder_thread = MonitorFolder(2, "watch_folder_thread", self.path_to_aut, path_of_folder_to_watch)
                monitor_folder_thread.start()
            elif 'monitor os' in test_step.lower():
                os_monitor.start()
            elif 'create directories' in test_step.lower():
                step, directory_path_to_create = test_step.split("::")
                make_sure_folder_exists(directory_path_to_create)
            elif 'delete directory' in test_step.lower():
                step, directory_path_to_remove = test_step.split("::")
                os.remove(directory_path_to_remove)
            elif 'edit file content' in test_step.lower():
                step, folder_path,num_files_to_edit = test_step.split("::")
                self.edit_file_content(folder_path,num_files_to_edit)
            elif 'edit file name' in test_step.lower():
                step, directory_path_to_create = test_step.split("::")
            elif 'edit file extension' in test_step.lower():
                step, old_extension, new_extension = test_step.split("::")
            elif 'wait for time' in test_step.lower():
                step, time_time_wait = test_step.split("::")
                time.sleep(int(time_time_wait))
            else:
                print("ERROR: Skipping test id: " + test_id + " because of unrecognized action")
                break
        # Collect and analyze results
        all_monitor_folder_results = monitor_folder_thread.get_monitor_folder_results()
        avrg_cpu_usage, avrg_mem_usage = os_monitor.get_average_os_statistics()
        os_monitor.stop()
        monitor_folder_thread.stop()

        test_case_results, files_test_results_added, files_test_results_changed = \
            analyze_results.analyze_after_file_creation(all_monitor_folder_results, num_files_to_generate)
        test_case_results["Test_case_ID"] = test_id
        test_case_results["test_case_name"] = test_name
        test_case_results["num_files_in_test"] = len(files_test_results_added) + len(files_test_results_changed)
        test_case_results["CPU_usage"] = avrg_cpu_usage
        test_case_results["mem_usage"] = avrg_mem_usage
        files_test_results = []
        files_test_results.extend(files_test_results_added)
        files_test_results.extend(files_test_results_changed)
        return test_case_results, files_test_results


class MonitorOS(threading.Thread):
    def __init__(self, threadID, name, current_os):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.current_os = current_os

    def run(self):
        self.continue_monitoring = True
        self.CPU_usage = []
        self.mem_usage = []
        process = filter(lambda p: p.name() == "index-win.exe", psutil.process_iter())
        for i in process:
            aut_cpu = psutil.Process(i.pid)
        self.continue_monitoring_CPU = True
        while self.continue_monitoring_CPU == True:
            CPU_usage =  aut_cpu.cpu_percent()
            mem_usage = int(aut_cpu.memory_percent()*100)
            print("CPU: " + str(CPU_usage) + "%")#str(aut_cpu.cpu_percent()))
            self.CPU_usage.append(CPU_usage)
            print("Mem: "+ str(mem_usage) + "%")#str(int(aut_cpu.memory_percent()*100))+"%")
            self.mem_usage.append(mem_usage)
            time.sleep(0.5)
        # print("Finished monitoring CPU & Mem")

    def get_average_os_statistics(self):
        # first remove any reading where the CPU usage of the process of  the AUT is 0 since that is either
        # after or before it was working. Memory reading of the same indexes can be removed as well.
        i = 0
        relevant_CPU_usage = self.CPU_usage
        relevant_mem_usage =  self.mem_usage
        total_cpu = 0
        total_mem = 0
        j = 0
        for i in range(1,len(relevant_CPU_usage)):
            if relevant_CPU_usage[i] > 0:
                total_cpu = total_cpu + relevant_CPU_usage[i]
                total_mem = total_mem + relevant_mem_usage[i]
                j += 1

        if j > 0:
            avrg_cpu = round(total_cpu / j,1)
            avrg_mem = total_mem / j
            return avrg_cpu, avrg_mem
        else:
            print("ERROR: reporting os parameters.")

            return 0, 0

    def get_latest_os_statistics(self):
        return self.CPU_usage[len(self.CPU_usage)-1], self.mem_usage[len(self.mem_usage)-1]

    def stop(self):
        # print("Stopping to monitor CPU & Mem")
        self.continue_monitoring_CPU = False


class MonitorFolder(threading.Thread):
    def __init__(self, threadID, name, path_to_aut, folder_to_watch):
        threading.Thread.__init__(self)
        self._stopevent = threading.Event()
        self.threadID = threadID
        self.name = name
        self.folder_to_watch = folder_to_watch
        self.path_to_aut = path_to_aut

    def get_monitor_folder_results(self):
        return self.all_monitoring_folder_results

    def run(self):
        # print("Monitoring folder: " + self.folder_to_watch)
        command = self.path_to_aut + ' -w ' + self.folder_to_watch
        # print(command)
        self.monitor_folder_results = copy.deepcopy(MONITOR_FOLDER_DICT)
        self.all_monitoring_folder_results =[]
        shell_output = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        results_counter = 0
        self.continue_monitoring_folder = True
        #while shell_output.poll() is None and self.continue_monitoring_folder:
        while self.continue_monitoring_folder:
            time.sleep(0.1)
            line = shell_output.stdout.readline()
            if "::!" in line:
                results_counter += 1
                self.monitor_folder_results["timestamp"], \
                self.monitor_folder_results["action"], \
                self.monitor_folder_results["file_path"] = line.split("::")
                self.all_monitoring_folder_results.append(self.monitor_folder_results.copy())
                # print("Found result #: " + str(results_counter))
                print(line)

        # print("finished monitoring folder: " + self.folder_to_watch + ", " + str(self.continue_monitoring_folder))

    def stop(self):
        # print("Stopping monitoring of folder: " + self.folder_to_watch)
        self.continue_monitoring_folder = False


def main(cmd_arguments):
    # Initiating params
    # print("Started test-setup")
    # in case there is something that needs to be handled differently per Operating system
    if 'linux' in sys.platform:
        current_os = 'Linux'
        aut_path = cmd_arguments[1] + "\\node10-linux-x64\\index-linux"
    elif 'win32' in sys.platform:
        current_os = 'Windows'
        aut_path = cmd_arguments[1] + "\\node10-win-x64\\index-win.exe"
    elif 'darwin' in sys.platform:
        current_os = "Mac OS X"
        aut_path = cmd_arguments[1] + "\\node10-macos-x64\\index-macos"
    else:
        current_os = 'OTHER'


    path_to_tests_cases = cmd_arguments[2]
    if os.path.exists(aut_path) and os.path.exists(path_to_tests_cases):



        test = ExecuteTestCases(aut_path, current_os)
        f = open(path_to_tests_cases, 'r')
        lines = f.readlines()
        f.close()
        log_path, file_name = os.path.split(path_to_tests_cases)
        excel_log = ExcelHandler("silverbolt_test_results")
        headers = copy.deepcopy(TEST_CASE_HEADER_DICT)
        for line in lines:
            if line[:1] != "#":
                headers['Test_case_ID'], headers['test_case_name'], test_case_steps_and_params = line.split(",", 2)
                excel_log.run_log_printing(headers, TEST_CASE_RESULTS_TUPLE, "TC results Summary")
        excel_log.save_workbook()
        excel_log.reset_indexes('TC results Summary')
        for line in lines:
            if line[:1] != "#":
                test_case_id, test_case_name, test_case_steps_and_params = line.split(",",2)
                test_case_results, files_test_results = test.run(test_case_id, test_case_name, test_case_steps_and_params.replace("\n",""))

                for file_test_results in files_test_results:
                    file_test_results["Test_case_ID"] = test_case_id
                    excel_log.run_log_printing(file_test_results, FILE_RESULTS_TUPLE, "File results Details")
                excel_log.run_log_printing(test_case_results, TEST_CASE_RESULTS_TUPLE, "TC results Summary")
                excel_log.save_workbook()
    else:
        print("ERROR: Can't find AUT  or file of test cases - " + path_to_tests_cases)

    print("Test Excution COMPLEAT!")


if __name__ == "__main__":
    main(sys.argv)
    sys.exit(0)
