## Test Engineer - Code Task

---

#### Description

We are a cyber defense software and service company.  
As you know in the cyber world all that can go wrong will go wrong.  
We need to test our products with many cases as required, the only way to do that is by automation.  
The following task is to demonstrate the style of tasks you will need to handle here (at least some of them).  
We generated a simple application that you require to test.  
The application will watch a specific folder to file changes and will look for *dirty* files (more info in the application help).  

To run the tested application:

* Using compiled code

  1. Go to the right distribution under "dist" folder
  2. Download the relevant executable

* From source code

  1. Clone the repository
  2. Open console in the cloned folder
  3. Run "npm install"
  4. Run "node ." to see the application CLI options
---

#### What we look for

* The following are important guidelines and artifacts we will review 

  1. An ordered test plan you will cover in automation
  2. An automation tool with clear guidelines on how to run it
  3. Tool should be cross-platform
  4. Automation tool should generate a summary report

* The summary report should contain the following content

  1. Test case coverage (fail/pass for each test case)
  2. CPU and Memory usage over time of the application on each test case
  3. Average and median handling time per file on test case
  4. % false positive (clean files that application removed)
  5. Accuracy (dirty files application removed)  

---

#### Preferred solution

* Our technology stack is the following

1. Node.Js
2. Python
3. C/C++

* You are free to choose any technology to implement the automation tool.
Preferable is one of the above.

* Test plan format can be a simple XL, we are more focused on the automation itself.

* World is changing, automation tool design should take future maintenance into account.

---
#### Good Luck!
 