# README #
## Test Engineer - Code Task for Silverbolt - By Joshua Weiss


### This repository contains: ###

A test tool that is meant to test an application that monitors folders
to catch files that are added or changed and might be harmfull to the system

The test tool is built to be multi platform and flexible allowing users to add
and modify test cases by simply editing a CSV file without any need to change the code of the tool itself

the version of this tool is a working proof of concept but is not yet a 100% compleate product.

### setting things up ###

This tool uses Python 2.7
you must also have excel on the computer for test results.

*Clone the repository
*Run the test tool giving it 2 parameters of the file location of the Application under test (AUT) and path to the CSV file of test cases
**Python .\Test_Suite\test.py <path to AUT> <path to CSV>



### Owner ###

* jrweiss01@gmail.com